# Begonecord

Heyo!

You may have seen my [toot](https://queer.party/@ao/100921380687423070) or videos ([1](https://www.youtube.com/watch?v=12x77PDFjNI), [2](https://www.youtube.com/watch?v=RDN6cn5-byc)) on the DRM bypass of Discord Store.

If you didn't, here's a quick gif of the PoC, running a paid game on an offline Windows system using a ticket for a free game:

![GIF demonstration of the DRM2 Bypass PoC](https://gitlab.com/ao/begonecord_exploit/raw/master/Assets/begonecord-drm2-poc.gif)

([WEBM with better resolution available here](https://gitlab.com/ao/begonecord_exploit/raw/master/Assets/begonecord-drm2-poc.webm))

([Here's Jesus' take on explaining it](https://peertube.video/videos/watch/ce4942da-c14a-4add-acf2-c6c839ee7843))

---

Firstly, I suck at writing stuff and tend to get confusing and rambly, so I apologize. If you have any questions, please don't hesitate. I can be reached at `a@a3.pm` on XMPP.

This is the repo that contains the stuff I found and stuff I wrote. I reported the DRM1-related sections of this to Discord, but the response I got was not helpful (they stated that they were working on DRM2 and gave me a date, but it got released a week after the date they said it'd be released, I wasn't given access to the game developer side of the SDK even though I requested access so that I could test for vulnerabilities, they took a lot to reply and messages were short, and my request of being simply informed on SDK2 release was declined). I wasn't going to email them about DRM2, but emailed them at the last minute saying that I'll release this in 24 hours with details on the issues I found on DRM2 on every single link of the security chain.

This repo was publicized on Nov 6, which 4 days after the date I initially gave to Discord when I reported the DRM1 exploits and on the date I gave to discord for DRM2 exploits.

---

## My Findings

Discord Game SDK relies on a dll file called `discord_aegis` (will be referred to as `aegis`). Aegis is found via registry by the patched game in `HKCU/Software/Discord/Modules/discord_aegis/Path_x64` or the same place, but `Path_x86`, depending on if the game's executable is 32bit or 64bit.

After finding Aegis, game loads it and calls `Aegis_Main` function in it. `Aegis_Main` looks for `discord_game_sdk` (will be referred to as `gamesdk`) at `HKCU/Software/Discord/Modules/discord_game_sdk/Path_(x86|x64)`, loads it and calls `DiscordCreate`. `DiscordCreate` is the function that handles DRM.

`DiscordCreate` then goes through various RPC pipes, from `\\?\pipe\discord-ipc-0` to `\\?\pipe\discord-ipc-9` until it finds one that exists. By launching begonecord before our discord client, we can take over an earlier RPC pipe and handle requests, or even MITM them.

First version of `gamesdk` asks for `VALIDATE_APPLICATION` (`{"cmd":"VALIDATE_APPLICATION","nonce":"1","evt":null,"args":null}`, will be referred to as DRM1) while second one asks for `GET_ENTITLEMENT_TICKET` (`{"cmd":"GET_ENTITLEMENT_TICKET","nonce":"1","evt":null,"args":null}`, will be referred to as DRM2). While DRM1 is really easy to bypass, my current method of bypassing DRM2 is a bit more complicated, though I'm working on making it easier.

To return to DRM1, simply replacing the `gamesdk` dll with the first version of gamesdk will suffice. Sadly I can't share it here as that'd violate the Discord ToS.

For `VALIDATE_APPLICATION`, discord client calls the `/entitlements` endpoint ([more info here](https://gitlab.com/ao/begonecord_exploit/blob/master/StoreDocs/API/store-endpoints.md#get-apiv6usersmeapplicationsappidentitlements)) and returns something similar to `{"cmd":"VALIDATE_APPLICATION","data":null,"evt":null,"nonce":"1"}` if you own the game. If you don't own the game, this returns smth similar to the previous response, `evt:ERROR` and data contains information about the error.

For `GET_ENTITLEMENT_TICKET`, discord client calls the `/entitlement-ticket` endpoint ([more info here](https://gitlab.com/ao/begonecord_exploit/blob/master/StoreDocs/API/store-endpoints.md#post-apiv6usersmeapplicationsappidentitlement-ticket)). Discord then passes this onto the game as `{"cmd":"GET_ENTITLEMENT_TICKET","data":<response from the endpoint>,"evt":null,"nonce":"1"}` if you own the game. If you don't, discord returns `{"cmd":"GET_ENTITLEMENT_TICKET","data":{"code":1000,"message":"Unknown Error"},"evt":"ERROR","nonce":"1"}`.

Entitlement tickets are static and are specific to the userid given on the `DISPATCH` call.

One simple way to ensure that you can play your games offline or even when discord goes bankrupt one day is to get all your entitlement tickets, back up your game files (they're stored in `%APPDATALOCAL%/DiscordGames`) and the discord `aegis`/`gamesdk` dlls (they're stored in `%APPDATA%/discord/0.0.<number>/modules/discord_modules/`). That way you can use the DRM2 bypass PyScript with your tickets to play your rightfully purchased games after placing the right registry entries.

However, if you forget to backup all of your entitlement tickets or you get banned from Discord etc before you get around to backing up your tickets, you can still bypass it by simply getting a ticket for a free game and then patching the APPID in the game executable. ~~Nice fuckup, Discord.~~ You may use `Scripts/PatchBinary` for this, or do it manually by following [AppIdPatchInstructions.md](https://gitlab.com/ao/begonecord_exploit/blob/master/Bypass/DRM2/AppIdPatchInstructions.md).

---

## List of materials

- StoreDocs/API: API Docs of various Discord Store endpoints
- Scripts/AddAll: Python script to add all Nitro games to account
- Scripts/BackupTickets: Python script to backup tickets for the games you own
- Scripts/LaunchWithBackedUpTickets: Python script to launch games using your backed up tickets
- Scripts/PatchBinary: Python script to patch binaries of games to replace the appid
- Bypass/DRM2/PyScript: Python script to bypass DRM without Discord client on.
- Bypass/DRM2/FAQ.md: Some questions you might have about DRM2, answered.
- Bypass/DRM2/AppIdPatchInstructions.md: Some questions you might have about DRM2, answered.
- Bypass/DRM1/PyScript: Python script to bypass DRM without Discord client on (as seen in the [second video](https://www.youtube.com/watch?v=RDN6cn5-byc))
- Bypass/DRM1/Rewrite: Rewrite information to bypass DRM with Discord client on (as seen in the [first video](https://www.youtube.com/watch?v=12x77PDFjNI))
- Bypass/example-registry-setup.reg: Example Discord registry setup from my system. Editing that to have your username and putting the right DLLs in those folders should help you bypass DRM in the future.
- Assets/: Small images and videos of the PoC of the exploit.

---

## Thanks

While most stuff I documented here are found by me, I want to thank the following people for helping me out along the way. I couldn't get all this stuff together without your help.

- Mary - @Mstrodl

---

Please do not use this for illegal purposes, I don't want Jake to yell at me in emails.

