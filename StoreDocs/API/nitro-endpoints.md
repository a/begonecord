# Nitro Endpoints

**This file is a part of Begonecord by ao.**

Unless specified otherwise, all calls are to https://discordapp.com/.

### GET `/api/v6/users/@me/billing/payment-sources`

- Requires authentication.

Returns the payment sources of the user.

#### Output

HTTP 200

```js
[
    { // Newer address, stuff isn't nulled
        "billing_address": {
            "city": "Example City",
            "name": "Jane Doe",
            "country": "NO",
            "state": "Example State",
            "postal_code": "12345",
            "line_1": "Example st",
            "line_2": "Example apt."
        },
        "default": false,
        "expires_year": 1337, // CC expiry year
        "brand": "Visa", // Can be MasterCard", "Visa", "Discover", "Diners Club", "American Express", "UnionPay" and "Maestro" (and also maybe PayPal and Unknown)
        "invalid": false,
        "last_4": "1337", // last 4 numbers of CC number
        "type": 1, // 1 = CC, 2 = Paypal
        "id": "500000000000000000", // ID of the payment source
        "expires_month": 1 // CC expiry month
    },
    { // Older address, most stuff are nulled
        "billing_address": {
            "city": null,
            "name": null,
            "country": "NO",
            "state": null,
            "postal_code": "12345",
            "line_1": null,
            "line_2": null
        },
        "default": false,
        "expires_year": 1337,
        "brand": "Visa",
        "invalid": false,
        "last_4": "1337",
        "type": 1,
        "id": "450000000000000000",
        "expires_month": 1
    }
]
```

### GET `/api/v6/users/@me/billing/payments`

- Requires authentication.

Shows Nitro subscription history.

#### Output

HTTP 200

```js
[
    {
        "status": 1,
        "amount_refunded": 0,
        "description": "Nitro Monthly",
        "tax": 0,
        "currency": "usd",
        "id": "500000000000000000",
        "subscription": {
            "payment_gateway_plan_id": "premium_month_tier_2",
            "current_period_start": "2018-10-18T07:30:47+00:00",
            "current_period_end": "2018-11-18T07:30:47+00:00",
            "type": 1,
            "id": "500000000000000000",
            "payment_gateway": 1
        },
        "tax_inclusive": true,
        "created_at": "2018-10-18T07:30:47+00:00",
        "payment_source": {
            "billing_address": {
                "city": "Example City",
                "name": "Jane Doe",
                "country": "NO",
                "state": "Example State",
                "postal_code": "12345",
                "line_1": "Example st",
                "line_2": "Example apt."
            },
            "expires_year": 1337,
            "brand": "Visa",
            "invalid": false,
            "last_4": "1337",
            "type": 1,
            "id": "500000000000000000",
            "expires_month": 1
        },
        "amount": 999, // Price, divide by 100
        "metadata": {}
    },
    {
        <more entries like the one above>
    }
]
```

### GET `/api/v6/users/@me/billing/subscriptions`

- Requires authentication.

Shows active Nitro subscriptions.

TODO: Can we find an endpoint that shows us the available nitro types?

#### Output

If we have nitro:

```js
[
{
    "status": 1, // 1 = active, 3 = cancelled
    "current_period_start": "2018-10-18T07:30:47+00:00",
    "payment_gateway_plan_id": "premium_month_tier_2", // 2 is for Nitro, 1 is for Nitro Classic / Lightro
    "payment_source_id": "450000000000000000",
    "canceled_at": null,
    "created_at": "2018-10-18T07:30:47+00:00",
    "type": 1, // hmm, perhaps difference between purchase and trial from event code?
    "id": "500000000000000000",
    "payment_gateway": 1, // 1 = CC, 2 = PayPal
    "current_period_end": "2018-11-18T07:30:47+00:00"
}
]
```

If we don't:

```js
[]
```

### POST `/api/v6/users/@me/billing/subscriptions`

- Requires authentication.
- Requires `Content-Type: application/json`
- Be warned that this may cost money to you. 

Attempts to subscribe.

#### Input

```js
{
  "payment_gateway_plan_id": "premium_month_tier_2", // 2 is for Nitro, 1 is for Nitro Classic / Lightro
  "payment_source_id": "450000000000000000"
}
```

#### Output

```js
{
    "status": 1,
    "current_period_start": "2018-10-18T07:30:47+00:00",
    "payment_gateway_plan_id": "premium_month_tier_2",
    "payment_source_id": "450000000000000000",
    "canceled_at": null,
    "created_at": "2018-10-18T07:30:47+00:00",
    "type": 1, // hmm, perhaps difference between purchase and trial from event code?
    "id": "500000000000000000",
    "payment_gateway": 1, // Explained above
    "current_period_end": "2018-11-18T07:30:47+00:00"
}
```

### DELETE `/api/v6/users/@me/billing/subscriptions/$subscriptionid`

- Requires authentication.

Cancels a subscription.

#### Output

**If successful:**

```js
 
```

(yes, that's empty, that wasn't very RESTful of you, discord)

**If already cancelled:**

```js
{
    "code": 100007,
    "message": "Already canceled"
}
```

### PATCH `/api/v6/users/@me/billing/subscriptions/$subscriptionid`

- Requires authentication.
- Requires `Content-Type: application/json`.

#### Input

```js
{
    "status":1,
    "payment_source_id":"450000000000000000"
}
```

#### Output

```js
{
    "status": 1,
    "current_period_start": "2018-10-18T07:30:47+00:00",
    "payment_gateway_plan_id": "premium_month_tier_2",
    "payment_source_id": "450000000000000000",
    "canceled_at": null,
    "created_at": "2018-10-18T07:30:47+00:00",
    "type": 1,
    "id": "500000000000000000",
    "payment_gateway": 1,
    "current_period_end": "2018-11-18T07:30:47+00:00"
}
```

