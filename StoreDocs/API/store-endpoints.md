# Store Endpoints

**This file is a part of Begonecord by ao.**

Unless specified otherwise, all calls are to https://discordapp.com/.

#### GET `/api/v6/users/@me/applications/$appid/entitlements`

- Requires authentication
- Requires you to own the game
- Called by Discord client first time you run a game after launching client
- Can be patched to bypass DRM with client open :)

Checks if you have an entitlement for a game.

#### Output

If you have an entitlement (you own the game):

```js
[
    {
        "user_id": "100000000000000000", // $userid
        "sku_id": "489914768048783390", // $skuid
        "application_id": "363414030188150801", // $appid
        "id": "500000000000000000", // $entitlementid, not used anywhere ATM
        "type": 2 // See the "About Entitlement Types" section below
    }
]
```

If you don't have an entitlement:

```js
[]
```

#### POST `/api/v6/users/@me/applications/$appid/entitlement-ticket`

- Requires authentication
- Requires you to own the game
- Called by Discord client first time you run a game after launching client

Returns your entitlement ticket for a game.

#### Output

If you have an entitlement (you own the game):

HTTP 200

```js
{
    "ticket": "1$[redacted]$eyJ1c2VyX2lkIjogNTAwMDAwMDAwMDAwMDAwMDAwLCAiY3JlYXRlZF9hdCI6ICIyMDE4LTExLTA0VDAwOjAxOjIzLjQ1Njc4OSIsICJhcHBsaWNhdGlvbl9pZCI6IDQ0ODQxNzg1MDQ2Njc2Mjc1M30="
}
```

First section is a 128 char hexadecimal, containing the signature for the other set of values.

Second section is a 152 char base64 string, containing something like this:

```js
{"user_id": 500000000000000000, "created_at": "2018-11-04T00:01:23.456789", "application_id": 448417850466762753}
```

`user_id` is checked against the DISPATCH given by client (or begonecord pyscript) and `application_id` is checked against the appid embedded in the binary, which can easily be patched with a hex editor. See `Bypass/DRM2/AppIdPatchInstructions.md`.

If you don't have an entitlement:

HTTP 404

```js
{
    "code": 10029,
    "message": "Unknown Entitlement"
}
```

### GET `/api/v6/users/@me/library`

- Requires Authentication.
- Called by Discord client at client launch
- Can be patched to bypass DRM with client open :)

Shows information about owned games.

```js
[
    {
        "application": {
            "executables": [
                {
                    "os": "win32",
                    "name": "love.exe"
                },
                {
                    "os": "win32",
                    "name": "win/love.exe"
                }
            ],
            "publishers": [
                "Those Awesome Guys"
            ],
            "application_id": "343232907390746625",
            "name": "Move or Die",
            "distributor_applications": [
                {
                    "distributor": "discord",
                    "sku": "491699415904550923"
                },
                {
                    "distributor": "steam",
                    "sku": "323850"
                }
            ],
            "summary": "Move or Die is an absurdly fast-paced, 4-player local and online party game where the mechanics change every 20 seconds. The very definition of a perfect friendship-ruining game.",
            "splash": "03f12f7e2982f66bb6f038c67374200c",
            "developers": [
                "Those Awesome Guys"
            ],
            "id": "343232907390746625",
            "icon": "a2f62440ab2e5e6a9971b4c170551841"
        },
        "created_at": "2018-10-25T09:00:11.800000+00:00",
        "sku_id": "491699415904550923",
        "flags": 24,
        "branch_id": "343232907390746625"
    },
    {
        "application": {
            "executables": [
                {
                    "os": "win32",
                    "name": "monaco.exe"
                },
                {
                    "os": "win32",
                    "name": "monaco/monaco.exe"
                }
            ],
            "publishers": [
                "Pocketwatch Games"
            ],
            "name": "Monaco",
            "distributor_applications": [
                {
                    "distributor": "discord",
                    "sku": "491665417933291551"
                },
                {
                    "distributor": "steam",
                    "sku": "113020"
                }
            ],
            "summary": "Monaco: What's Yours Is Mine is a single player or co-op heist game.  Assemble a crack team of thieves, case the joint, and pull off the perfect heist.",
            "splash": "5253c177a9d7f16dc56838a27a592430",
            "icon": "5d6a28659e629816992a3c86864e7a2c",
            "id": "358421725139959808",
            "aliases": [
                "Monaco: What's Yours Is Mine"
            ]
        },
        "created_at": "2018-10-25T08:57:20.624000+00:00",
        "sku_id": "491665417933291551",
        "flags": 24,
        "branch_id": "358421725139959808"
    }
]
```

### POST `/api/v6/store/skus/$skuid/purchase`

- Requires authentication.
- Requires `Content-Type: application/json`
- No input (at least for Nitro games).

Purchases a game.

#### Output

**If already owned:**

```js
{
    "code": 100011,
    "message": "Already purchased"
}
```

**If successfully purchased:**

```js
{
    "entitlements": [
        {
            "user_id": "100000000000000000",
            "sku_id": "489914768048783390", // $skuid
            "application_id": "363414030188150801", // $appid
            "id": "500000000000000000", // $entitlementid
            "type": 2
        }
    ],
    "library_applications": [
        {
            "application": {
                "executables": [
                    {
                        "os": "win32",
                        "name": "ftl faster than light/ftlgame.exe"
                    }
                ],
                "publishers": [
                    "Subset Games"
                ],
                "name": "FTL: Faster Than Light",
                "distributor_applications": [
                    {
                        "distributor": "discord",
                        "sku": "489914768048783390" // $skuid
                    },
                    {
                        "distributor": "steam",
                        "sku": "212680"
                    }
                ],
                "summary": "You've got a spaceship, a crew, and a whole new galaxy to explore. Experience the thrill of trying not to break anything while hurtling through space!",
                "splash": "8606b26658527ed811aebf152cd77b3b",
                "developers": [
                    "Subset Games"
                ],
                "id": "363414030188150801", // $appid
                "icon": "6bb701757ad6232284893750cb3c5d08"
            },
            "created_at": "2018-10-18T07:43:37.534995+00:00",
            "sku_id": "489914768048783390", // $skuid
            "flags": 24,
            "branch_id": "363414030188150801" // $branchid
        }
    ]
}
```

### GET `/api/v6/store/published-listings/applications?application_ids=$id`

- Authentication not needed.
- Multiple application ids can be passed to the call, separated with a comma.

Returns information about the published store listing specified from the application ID. Useful for finding SKU ID from Application ID.

#### Output

```js
[
    {
        "sku": {
            "premium": true, // true -> free for nitro, false -> paid game
            "features": [  // See the "About Features" section below
                1,
                10,
                11
            ],
            "access_type": 1, // See the "About Entitlement Types" section below
            "show_age_gate": false,
            "application_id": "425777385103949834", // Application ID
            "slug": "super-meat-boy",
            "locales": [
                "en-GB",
                "ru",
                "en-US"
            ],
            "genres": [ // See the "About Genres" section below
                1,
                5
            ],
            "name": "Super Meat Boy",
            "manifest_labels": [
                "476899846742343689"
            ],
            "dependent_sku_id": null, // For expansions?
            "release_date": "2010-10-20",
            "id": "471500856043372544", // SKU ID
            "type": 1, // See the "About SKU Types" section below
            "system_requirements": {
                "1": { // See the "About OS Types" section below
                    "recommended": {
                        "sound_card": null,
                        "operating_system_version": null,
                        "disk": 1000,
                        "network": null,
                        "gpu": "Pixel Shader 3.0, Vertex Shader 3.0",
                        "notes": null,
                        "ram": 1024,
                        "cpu": "2.0 GHz",
                        "directx": "Version 9.0c"
                    },
                    "minimum": {
                        "sound_card": null,
                        "operating_system_version": null,
                        "disk": 1000,
                        "network": null,
                        "gpu": "Pixel Shader 3.0, Vertex Shader 3.0",
                        "notes": null,
                        "ram": 1024,
                        "cpu": "1.4 GHz",
                        "directx": "9.0c"
                    }
                }
            }
        },
        "flavor_text": "Juicy platforming",
        "tagline": "A challenging 2D platformer with precise controls. Play as a boy made of meat and rescue your girlfriend, bandage girl.",
        "preview_video": {
            "height": 360,
            "width": 640,
            "filename": "SuperMeatBoy.mp4",
            "id": "499349685308227585",
            "mime_type": "video/quicktime",
            "size": 996997
        },
        "id": "471500857100468237",
        "summary": "Super Meat Boy is a tough as nails platformer where you play as an animated cube of meat who's trying to save his girlfriend (who happens to be made of bandages) from an evil fetus in a jar wearing a tux.",
        "thumbnail": {
            "height": 708,
            "width": 1260,
            "filename": "425777385103949834.png",
            "id": "475580511738134528",
            "mime_type": "image/png",
            "size": 468270
        }
    },
    {
        "sku": {
            "exclusive": true, // "First on discord"
            "premium": false, // AKA, a "Store Game", aka it's paid
            "features": [
                1,
                2,
                3,
                4,
                6,
                7,
                8,
                10,
                11
            ],
            "price": { // Divide amount by 100 to get the correct price
                "currency": "usd",
                "amount": 1499
            },
            "access_type": 1,
            "legal_notice": "©2018 Mild Beast Games. Versus Evil and the Versus Evil logo are trademarks of Versus Evil LLC. All other trademarks and logos are property of their respective owners. All rights reserved.",
            "show_age_gate": false,
            "application_id": "379732021385232385",
            "slug": "at-sundown-tm",
            "locales": [
                "ru",
                "fr",
                "en-US",
                "de",
                "ko",
                "it",
                "zh-CN",
                "pt-BR",
                "es-ES",
                "ja",
                "pl"
            ],
            "genres": [
                27,
                23
            ],
            "name": "AT SUNDOWN™",
            "manifest_labels": [
                "487025944264048680"
            ],
            "dependent_sku_id": null,
            "release_date": "2018-10-16",
            "content_rating": {
                "rating": 3,
                "descriptors": [
                    1
                ]
            },
            "id": "487031053454802946",
            "content_rating_agency": 2,
            "type": 1,
            "system_requirements": {
                "1": {
                    "recommended": {
                        "sound_card": null,
                        "operating_system_version": null,
                        "disk": 2000,
                        "network": null,
                        "gpu": "Nvidia GeForce GTX 780 or ATi Radeon R5",
                        "notes": null,
                        "ram": 8000,
                        "cpu": "Intel Core i3 or AMD Phenom II X4 965",
                        "directx": "Version 10"
                    },
                    "minimum": {
                        "sound_card": null,
                        "operating_system_version": null,
                        "disk": 2000,
                        "network": null,
                        "gpu": "Intel Integrated HD 4000",
                        "notes": null,
                        "ram": 4000,
                        "cpu": "Intel Core 2 Duo",
                        "directx": "Version 10"
                    }
                }
            }
        },
        "header_background": {
            "height": 1594,
            "width": 4500,
            "filename": "At_Sundown_-_Background.png",
            "id": "499991845598658560",
            "mime_type": "image/png",
            "size": 236199
        },
        "flavor_text": "N E O N. L A S E R S.",
        "tagline": "A hide-and-seek shooter where the maps are covered in darkness and light is your enemy, as four players compete in a deathmatch to be the best!",
        "header_logo_dark_theme": {
            "height": 200,
            "width": 786,
            "filename": "At_Sundown_Logo_TM_HD_white.png",
            "id": "501530752551354368",
            "mime_type": "image/png",
            "size": 11224
        },
        "preview_video": {
            "height": 360,
            "width": 640,
            "filename": "AtSundown_10-15sec_DiscordLoop_ffmpeg.mp4",
            "id": "501503544374067210",
            "mime_type": "video/quicktime",
            "size": 1676944
        },
        "id": "487031056239951893",
        "header_logo_light_theme": {
            "height": 200,
            "width": 786,
            "filename": "At_Sundown_Logo_TM_HD_black.png",
            "id": "501530753075511327",
            "mime_type": "image/png",
            "size": 9858
        },
        "summary": "A hide-and-seek shooter in the dark where light is your enemy, as 4 players compete in a deathmatch to be the best!",
        "thumbnail": {
            "height": 708,
            "width": 1260,
            "filename": "at-sundown-discord-tm_good_to_go.jpg",
            "id": "501496663635132416",
            "mime_type": "image/jpeg",
            "size": 150040
        }
    }
]
```

### GET `/api/v6/store/published-listings/skus/471500856043372544?country_code=$cc`

- Authentication not needed.

Returns information about the published store listing specified from the SKU ID. Useful for finding Application ID.

`country_code` is a two letter code, such as `US` or `TR`.

#### Output

```js
{
    "sku": {
        "premium": true,
        "features": [
            1,
            10,
            11
        ],
        "access_type": 1,
        "show_age_gate": false,
        "application_id": "425777385103949834", //Application ID
        "slug": "super-meat-boy",
        "locales": [
            "en-GB",
            "ru",
            "en-US"
        ],
        "genres": [
            1,
            5
        ],
        "name": "Super Meat Boy",
        "manifest_labels": [
            "476899846742343689"
        ],
        "dependent_sku_id": null,
        "release_date": "2010-10-20",
        "id": "471500856043372544", // SKU ID
        "reviews": [
            {
                "url": "https://www.metacritic.com/game/pc/super-meat-boy",
                "aggregator": 1,
                "critic_total_ratings": 11,
                "critic_positive_ratings": 11,
                "reviews": [
                    {
                        "url": "http://www.gamespot.com/pc/action/supermeatboy/review.html",
                        "quote": "This sublime platformer provides tons of challenge, precise controls, and incredible level design that will keep you glued to the screen.",
                        "score": 95,
                        "publisher": "GameSpot"
                    },
                    {
                        "url": "http://www.gamers.at/articles/super_meat_boy-1003/?szAction=show&nReceiverID=60&nID=1003&nTextPage=1&nGameID=6692",
                        "quote": "Oldschool-Jump and Run at its finest!",
                        "score": 90,
                        "publisher": "Gamers.at"
                    },
                    {
                        "url": "",
                        "quote": "As beautiful as it is hard. You'll love it and hate it at the same time, but ultimately walk away satisfied. [Feb 2011, p.59]",
                        "score": 90,
                        "publisher": "PC PowerPlay"
                    }
                ],
                "secondary_total_ratings": 817,
                "secondary_score": 8.3,
                "critic_score": 87
            }
        ],
        "type": 1,
        "system_requirements": {
            "1": {
                "recommended": {
                    "sound_card": null,
                    "operating_system_version": null,
                    "disk": 1000,
                    "network": null,
                    "gpu": "Pixel Shader 3.0, Vertex Shader 3.0",
                    "notes": null,
                    "ram": 1024,
                    "cpu": "2.0 GHz",
                    "directx": "Version 9.0c"
                },
                "minimum": {
                    "sound_card": null,
                    "operating_system_version": null,
                    "disk": 1000,
                    "network": null,
                    "gpu": "Pixel Shader 3.0, Vertex Shader 3.0",
                    "notes": null,
                    "ram": 1024,
                    "cpu": "1.4 GHz",
                    "directx": "9.0c"
                }
            }
        }
    },
    "description": "Our meaty hero will leap from walls, over seas of buzz saws, through crumbling caves and pools of old needles. Sacrificing his own well being to save his damsel in distress. Super Meat Boy brings the old school difficulty of classic NES titles like Mega Man 2, Ghost and Goblins and Super Mario Bros. 2 (The Japanese one) and stream lines them down to the essential no BS straight forward twitch reflex platforming.\n\nRamping up in difficulty from hard to soul crushing SMB will drag Meat boy though haunted hospitals, salt factories and even hell itself. And if 300+ single player levels weren't enough SMB also throws in epic boss fights, a level editor and tons of unlockable secrets, warp zones and hidden characters.\n\n# Features\n\n- Story mode, featuring over 300 levels spanning 5+ chapters.\n- Warp zones that will warp you into other games.\n- Over 16 unlockable able and playable characters from popular indie titles such as, Minecraft, Bit.Trip, VVVVVV and Machinarium.\n- Epic Boss fights.\n- Full Level Editor and Level Portal called Super Meat World!\n- A story so moving you will cry yourself to sleep for the rest of your life.\n",
    "tagline": "A challenging 2D platformer with precise controls. Play as a boy made of meat and rescue your girlfriend, bandage girl.",
    "staff_notes": { // "Discord Staff Pick"
        "content": "Indie legend Super Meat Boy is as fast-paced and frantic as it is tough. The difficulty of this 2D platformer ramps up enough to challenge, but paces well enough to stave off frustration after dying repeatedly. Which you will do. A lot.",
        "user": {
            "username": "Entemper",
            "discriminator": "0001",
            "id": "90602143374925824",
            "avatar": "a_a247e900f69360781132bf27be6f74fe"
        }
    },
    "preview_video": {
        "height": 360,
        "width": 640,
        "filename": "SuperMeatBoy.mp4",
        "id": "499349685308227585",
        "mime_type": "video/quicktime",
        "size": 996997
    },
    "flavor_text": "Juicy platforming",
    "thumbnail": {
        "height": 708,
        "width": 1260,
        "filename": "425777385103949834.png",
        "id": "475580511738134528",
        "mime_type": "image/png",
        "size": 468270
    },
    "assets": [
        {
            "height": 720,
            "width": 1280,
            "filename": "screen10.jpg",
            "id": "499029091870310400",
            "mime_type": "image/jpeg",
            "size": 428716
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen11.jpg",
            "id": "499029093669404672",
            "mime_type": "image/jpeg",
            "size": 336060
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen9.jpg",
            "id": "499029089928347650",
            "mime_type": "image/jpeg",
            "size": 129236
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen13.jpg",
            "id": "499029097289220096",
            "mime_type": "image/jpeg",
            "size": 237170
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen7.jpg",
            "id": "499029086329503756",
            "mime_type": "image/jpeg",
            "size": 235428
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen5.jpg",
            "id": "499029083200552970",
            "mime_type": "image/jpeg",
            "size": 352099
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen3.jpg",
            "id": "499029078897197067",
            "mime_type": "image/jpeg",
            "size": 436042
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen1.jpg",
            "id": "499029074614681612",
            "mime_type": "image/jpeg",
            "size": 312098
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "vid1.mp4",
            "id": "499080816467378178",
            "mime_type": "video/quicktime",
            "size": 9044608
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen8.jpg",
            "id": "499029088367935501",
            "mime_type": "image/jpeg",
            "size": 223264
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen6.jpg",
            "id": "499029084865822740",
            "mime_type": "image/jpeg",
            "size": 507153
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen2.jpg",
            "id": "499029076942782488",
            "mime_type": "image/jpeg",
            "size": 399635
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen12.jpg",
            "id": "499029095699447818",
            "mime_type": "image/jpeg",
            "size": 232983
        },
        {
            "height": 720,
            "width": 1280,
            "filename": "screen4.jpg",
            "id": "499029081279430686",
            "mime_type": "image/jpeg",
            "size": 418510
        }
    ],
    "summary": "Super Meat Boy is a tough as nails platformer where you play as an animated cube of meat who's trying to save his girlfriend (who happens to be made of bandages) from an evil fetus in a jar wearing a tux.",
    "id": "471500857100468237", // No clue what this is. It's not appid or skuid.
    "carousel_items": [
        {
            "asset_id": "499080816467378178"
        },
        {
            "asset_id": "499029074614681612"
        },
        {
            "asset_id": "499029076942782488"
        },
        {
            "asset_id": "499029078897197067"
        },
        {
            "asset_id": "499029081279430686"
        },
        {
            "asset_id": "499029083200552970"
        },
        {
            "asset_id": "499029084865822740"
        },
        {
            "asset_id": "499029086329503756"
        },
        {
            "asset_id": "499029088367935501"
        },
        {
            "asset_id": "499029089928347650"
        },
        {
            "asset_id": "499029091870310400"
        },
        {
            "asset_id": "499029093669404672"
        },
        {
            "asset_id": "499029095699447818"
        },
        {
            "asset_id": "499029097289220096"
        }
    ]
}
```

### POST `/api/v6/applications/$appid/branches/$branchid/builds/$buildid/size`

TODO: Find a way to get buildid and manifestid.

- Requires authentication.
- $branchid seems to be same as $appid

Returns size of a game's build.

TODO: perhaps see if this requires a game that has nitro / owns the game?

#### Input

```js
{
    "manifest_ids": ["$manifestid"]
}
```

#### Output

```js
{
    "size_kb": "332673"
}
```

### GET `/api/v6/oauth2/applications/$appid/rpc`

- Requires authentication

Returns some info about the game. Called when game is launched.

#### Output

```js
{
    "description": "",
    "icon": null,
    "flags": 2048, // ?
    "id": "425777385103949834",
    "name": "Super Meat Boy"
}
```

### GET `/api/v6/users/@me/activities/statistics/applications`

- Requires authentication.

Shows your play statistics. Durations are in seconds, I believe.

#### Output

```js
[
    {
        "last_played_at": "2018-04-13T00:46:01.415000+00:00",
        "application_id": "358422126602223616",
        "total_discord_sku_duration": 0, // Playtime through Discord Store
        "total_duration": 6089 // Total playtime
    },
    {
        "last_played_at": "2018-10-19T08:08:33.142000+00:00",
        "application_id": "425777385103949834",
        "total_discord_sku_duration": 1563,
        "total_duration": 1563
    },
    {
        "last_played_at": "2018-10-19T08:07:42.961000+00:00",
        "application_id": "491427187442974749",
        "total_discord_sku_duration": 24,
        "total_duration": 24
    }
]
```

### GET `/api/v6/game-news?game_ids=$appid`

- Authorization needed
- If we're going to be a pedant, this isn't a store endpoint, it's an activity endpoint. Y'know, that tab that no one uses (hint: you can disable it in Appearance settings). It can be used with any application with an ID, not just stuff that's on store.
- Example appid with news: 356875988589740042

Shows news about a given game.

#### Output

If game has no news:

```js
[]
```

If game has news:

```js
[
    {
        "description": "* Custom saved sets now include the selected style.",
        "title": "Dota 2 Update - September 18th, 2018",
        "url": "https://store.steampowered.com/news/44031/",
        "timestamp": "2018-09-18T23:51:00+00:00",
        "flags": 2, // See the "About News Flags" section below
        "game_id": "356875988589740042", // AppID
        "type": "rich",
        "thumbnail": {
            "url": "https://steamcdn-a.akamaihd.net/steam/apps/570/header.jpg?t=1536248487",
            "width": 460,
            "proxy_url": "https://images-ext-2.discordapp.net/external/ABTc1gibLyyQSn92wGdAll4o0Z26GOp5oAHMTaN-HIE/%3Ft%3D1536248487/https/steamcdn-a.akamaihd.net/steam/apps/570/header.jpg",
            "height": 215
        }
    },
    {
        "description": "* Fix an interaction between Windranger's Focus Fire and Windrun.",
        "title": "Dota 2 Update - September 27, 2018",
        "url": "https://store.steampowered.com/news/44368/",
        "timestamp": "2018-09-28T02:08:00+00:00",
        "flags": 2,
        "game_id": "356875988589740042",
        "type": "rich"
    }
]
```

### GET `/api/v6/store/directory/497223016783151165?country_code=$cc`

- No authentication needed.
- Country code is a 2 letter code, such as `US` or `TR`.
- Number determines the scheme. It's hardcoded in Discord (`t.STORE_DIRECTORY_LAYOUT_ID = "497223016783151165"`).

Shows all games in Discord Store.

#### Output

```js
{
    "data": {
        "premium": [ // Trimmed some games from here. These are nitro games.
            {
                "sku_id": "504468643384524800",
                "type": 1
            },
            {
                "sku_id": "471030867175669760",
                "type": 2
            }
        ],
        "featured": [ // Trimmed some games from here. These are featured games.
            {
                "sku_id": "487031053454802946",
                "type": 1
            },
            {
                "sku_id": "488607666231443456",
                "type": 2
            }
        ],
        "sku_ids": [ // Trimmed some $skuids from here.
            "489922243854336000",
            "489230107093893120"
        ],
        "hero": [ // no clue what this is, kept untouched
            {
                "sku_id": "488422292137836574",
                "type": 1
            },
            {
                "type": 2
            },
            {
                "sku_id": "487031053454802946",
                "type": 1
            },
            {
                "sku_id": "500428425362931713",
                "type": 1
            },
            {
                "sku_id": "489929019895316481",
                "type": 1
            }
        ],
        "premium_carousel": [ // Trimmed some games from here. These are displayed on Nitro page, advertising the free games.
            {
                "tagline": "4X strategy on an intergalactic scale",
                "sku_id": "498203870363910145"
            },
            {
                "tagline": "Intense tower wars in a dystopian future",
                "sku_id": "492004779459870724"
            }
        ]
    },
    "id": "497223016783151165", // scheme id?
    "store_listings": [
        {
            "sku": {
                "premium": true,
                "features": [
                    1
                ],
                "access_type": 1,
                "legal_notice": "inXile entertainment Inc., 2727 Newport Blvd., Newport Beach, CA 92663. Copyright 2018 inXile entertainment Inc., Wasteland, the Wasteland logos, and inxile entertainment and the inXile entertainment logo are trademarks or registered trademarks of inXile entertainment Inc. in the U.S. and/or other countries. Copyright 2002 - 2018, inXile entertainment, Inc. All Rights Reserved. Fallout is a registered trademark of Bethesda Softworks LLC",
                "show_age_gate": true,
                "application_id": "489917055282315274", // $appid
                "slug": "wasteland-2-director-s-cut",
                "locales": [
                    "ru",
                    "fr",
                    "de",
                    "en-US",
                    "tr",
                    "it",
                    "es-ES",
                    "pl"
                ],
                "genres": [
                    48,
                    20,
                    53
                ],
                "name": "Wasteland 2: Director's Cut",
                "manifest_labels": [
                    "492409029688295444"
                ],
                "dependent_sku_id": null,
                "release_date": "2014-09-19",
                "content_rating": {
                    "rating": 4,
                    "descriptors": [
                        8,
                        17,
                        4,
                        21,
                        29
                    ]
                },
                "id": "489922243854336000",
                "content_rating_agency": 1, // See "About Content Rating Agencies" below
                "type": 1,
                "system_requirements": {
                    "1": {
                        "recommended": {
                            "sound_card": null,
                            "operating_system_version": null,
                            "disk": 30720,
                            "network": null,
                            "gpu": "Nvidia GeForce GTX 460 or Radeon HD 5770 (1 GB VRAM)",
                            "notes": null,
                            "ram": 8000,
                            "cpu": "Intel i5 series or AMD equivalent",
                            "directx": "Version 10"
                        },
                        "minimum": {
                            "sound_card": null,
                            "operating_system_version": null,
                            "disk": 30720,
                            "network": null,
                            "gpu": "Nvidia GeForce GTX 260 or Radeon HD 4850 (512 MB VRAM)",
                            "notes": null,
                            "ram": 4000,
                            "cpu": "Intel Core 2 Duo or AMD equivalent",
                            "directx": "Version 9.0c"
                        }
                    }
                }
            },
            "tagline": "For the first time since the original Fallout games, this team reunites and delivers to us this post-apocalyptic strategy adventure revival.",
            "preview_video": {
                "height": 360,
                "width": 640,
                "filename": "Wasteland2DirectorsCut_10-15sec_DiscordLoop_ffmpeg.mp4",
                "id": "494334272811827210",
                "mime_type": "video/mp4",
                "size": 1745691
            },
            "flavor_text": "Apocalyptic cowboys",
            "thumbnail": {
                "height": 708,
                "width": 1260,
                "filename": "wasteland-2-directors-cut.jpg",
                "id": "492430019776217108",
                "mime_type": "image/jpeg",
                "size": 418566
            },
            "summary": "The Wasteland's hellish landscape is waiting for you to make your mark... or die trying.",
            "id": "489922245981110297"
        },
        {
            "sku": {
                "premium": false,
                "features": [
                    1,
                    10,
                    11
                ],
                "price": {
                    "currency": "usd",
                    "amount": 1999
                },
                "access_type": 1,
                "show_age_gate": false,
                "application_id": "454814894596816907",
                "slug": "celeste",
                "locales": [
                    "ru",
                    "fr",
                    "de",
                    "en-US",
                    "it",
                    "zh-CN",
                    "pt-BR",
                    "es-ES",
                    "ja"
                ],
                "genres": [
                    5
                ],
                "name": "Celeste",
                "manifest_labels": [
                    "501260648957739008"
                ],
                "dependent_sku_id": null,
                "release_date": "2018-01-25",
                "content_rating": {
                    "rating": 2,
                    "descriptors": [
                        9,
                        11,
                        1
                    ]
                },
                "id": "500428425362931713",
                "content_rating_agency": 1,
                "type": 1,
                "system_requirements": {
                    "1": {
                        "minimum": {
                            "sound_card": null,
                            "operating_system_version": null,
                            "disk": 1200,
                            "network": null,
                            "gpu": "Intel HD 4000",
                            "notes": null,
                            "ram": 2000,
                            "cpu": "Intel Core i3 M380",
                            "directx": "Version 10"
                        }
                    }
                }
            },
            "header_background": {
                "height": 540,
                "width": 960,
                "filename": "Header.png",
                "id": "500433490903433216",
                "mime_type": "image/png",
                "size": 745748
            },
            "tagline": "A gorgeous, fast-paced, breathlessly difficult twitch platformer with a story that's much deeper than just climbing a mountain.",
            "preview_video": {
                "height": 360,
                "width": 640,
                "filename": "Celeste_10-15sec_DiscordLoop_ffmpeg.mp4",
                "id": "501559234077523997",
                "mime_type": "video/quicktime",
                "size": 1394954
            },
            "flavor_text": "Find yourself on the mountain",
            "thumbnail": {
                "height": 720,
                "width": 1280,
                "filename": "celeste.jpg",
                "id": "501471491347709962",
                "mime_type": "image/jpeg",
                "size": 671335
            },
            "summary": "Brave hundreds of hand-crafted challenges, uncover devious secrets, and piece together the mystery of the mountain.",
            "id": "500428426327621663"
        }
    ]
}
```

### GET `/api/v6/applications`

- No authorization needed.

Shows all games recognized by Discord (to be displayed as "Playing x").

#### Output

```js
[ // This is trimmed significantly, there's 40k lines in original response.
    {
        "icon": null,
        "id": 0,
        "name": "Discord",
        "splash": null
    },
    {
        "application_id": "259392830932254721",
        "developers": [
            "DoubleDutch Games"
        ],
        "distributor_applications": [
            {
                "distributor": "steam",
                "sku": "207140"
            }
        ],
        "executables": [
            {
                "name": "speedrunners.exe",
                "os": "win32"
            },
            {
                "name": "speedrunners/speedrunners.exe",
                "os": "win32"
            }
        ],
        "icon": "cb48279ea90e86fb4f71c709d3236395",
        "id": "259392830932254721",
        "name": "SpeedRunners",
        "publishers": [
            "tinyBuild"
        ],
        "splash": "94e91cac9509fee1eb80a69b9503878a",
        "summary": "Run, jump, slide, and swing into this 4 player competitive platformer by DoubleDutch Games."
    }
]
```

### POST `/api/v6/users/@me/library/$appid/$branchid/installed`

- Requires authentication
- Called by client when a game install is done

Notifies discord that a game is installed. Likely used for telemetry.

### Output

```js
 
```

(It returns HTTP 204 and gives an empty result)

## About Features

This is the list of features, extracted from Discord's JS code.

```js
{
    SINGLE_PLAYER: 1,
    ONLINE_MULTIPLAYER: 2,
    LOCAL_MULTIPLAYER: 3,
    PVP: 4,
    LOCAL_COOP: 5,
    CROSS_PLATFORM: 6,
    RICH_PRESENCE: 7,
    DISCORD_GAME_INVITES: 8,
    SPECTATOR_MODE: 9,
    CONTROLLER_SUPPORT: 10,
    CLOUD_SAVES: 11,
    ONLINE_COOP: 12,
    SECURE_NETWORKING: 13
}
```

Here's what they point at:

```
APPLICATION_STORE_SINGLE_PLAYER: "Single Player",
APPLICATION_STORE_LOCAL_MULTIPLAYER: "Local Multiplayer",
APPLICATION_STORE_ONLINE_MULTIPLAYER: "Online Multiplayer",
APPLICATION_STORE_PVP: "PvP",
APPLICATION_STORE_LOCAL_COOP: "Local Cooperative",
APPLICATION_STORE_ONLINE_COOP: "Online Cooperative",
APPLICATION_STORE_CROSS_PLATFORM: "Cross Platform",
APPLICATION_STORE_RICH_PRESENCE: "Rich Presence",
APPLICATION_STORE_DISCORD_GAME_INVITES: "Discord Game Invites",
APPLICATION_STORE_SPECTATOR_MODE: "Spectator Mode",
APPLICATION_STORE_CONTROLLER_SUPPORT: "Controller Support",
APPLICATION_STORE_CLOUD_SAVES: "Cloud Saves",
APPLICATION_STORE_SECURE_NETWORKING: "Secure Networking",
```

## About Genres

This is the list of genres, extracted from Discord's JS code.

```js
{
    ACTION: 1,
    ACTION_RPG: 2,
    BRAWLER: 3,
    HACK_AND_SLASH: 4,
    PLATFORMER: 5,
    STEALTH: 6,
    SURVIVAL: 7,
    ADVENTURE: 8,
    ACTION_ADVENTURE: 9,
    METROIDVANIA: 10,
    OPEN_WORLD: 11,
    PSYCHOLOGICAL_HORROR: 12,
    SANDBOX: 13,
    SURVIVAL_HORROR: 14,
    VISUAL_NOVEL: 15,
    DRIVING_RACING: 16,
    VEHICULAR_COMBAT: 17,
    MASSIVELY_MULTIPLAYER: 18,
    MMORPG: 19,
    ROLE_PLAYING: 20,
    DUNGEON_CRAWLER: 21,
    ROGUELIKE: 22,
    SHOOTER: 23,
    LIGHT_GUN: 24,
    SHOOT_EM_UP: 25,
    FPS: 26,
    DUAL_JOYSTICK_SHOOTER: 27,
    SIMULATION: 28,
    FLIGHT_SIMULATOR: 29,
    TRAIN_SIMULATOR: 30,
    LIFE_SIMULATOR: 31,
    FISHING: 32,
    SPORTS: 33,
    BASEBALL: 34,
    BASKETBALL: 35,
    BILLIARDS: 36,
    BOWLING: 37,
    BOXING: 38,
    FOOTBALL: 39,
    GOLF: 40,
    HOCKEY: 41,
    SKATEBOARDING_SKATING: 42,
    SNOWBOARDING_SKIING: 43,
    SOCCER: 44,
    TRACK_FIELD: 45,
    SURFING_WAKEBOARDING: 46,
    WRESTLING: 47,
    STRATEGY: 48,
    FOUR_X: 49,
    ARTILLERY: 50,
    RTS: 51,
    TOWER_DEFENSE: 52,
    TURN_BASED_STRATEGY: 53,
    WARGAME: 54,
    MOBA: 55,
    FIGHTING: 56,
    PUZZLE: 57,
    CARD_GAME: 58,
    EDUCATION: 59,
    FITNESS: 60,
    GAMBLING: 61,
    MUSIC_RHYTHM: 62,
    PARTY_MINI_GAME: 63,
    PINBALL: 64,
    TRIVIA_BOARD_GAME: 65
}
```

Here's what they point at:

```
APPLICATION_STORE_GENRE_ACTION: "Action",
APPLICATION_STORE_GENRE_ACTION_RPG: "Action RPG",
APPLICATION_STORE_GENRE_BRAWLER: "Beat 'Em Up/Brawler",
APPLICATION_STORE_GENRE_HACK_AND_SLASH: "Hack and Slash",
APPLICATION_STORE_GENRE_PLATFORMER: "Platformer",
APPLICATION_STORE_GENRE_STEALTH: "Stealth",
APPLICATION_STORE_GENRE_SURVIVAL: "Survival",
APPLICATION_STORE_GENRE_ADVENTURE: "Adventure",
APPLICATION_STORE_GENRE_ACTION_ADVENTURE: "Action-Adventure",
APPLICATION_STORE_GENRE_METROIDVANIA: "Metroidvania",
APPLICATION_STORE_GENRE_OPEN_WORLD: "Open-World",
APPLICATION_STORE_GENRE_PSYCHOLOGICAL_HORROR: "Psychological Horror",
APPLICATION_STORE_GENRE_SANDBOX: "Sandbox",
APPLICATION_STORE_GENRE_SURVIVAL_HORROR: "Survival Horror",
APPLICATION_STORE_GENRE_VISUAL_NOVEL: "Visual Novel",
APPLICATION_STORE_GENRE_DRIVING_RACING: "Driving/Racing",
APPLICATION_STORE_GENRE_VEHICULAR_COMBAT: "Vehicular Combat",
APPLICATION_STORE_GENRE_MASSIVELY_MULTIPLAYER: "Massively Multiplayer",
APPLICATION_STORE_GENRE_MMORPG: "MMORPG",
APPLICATION_STORE_GENRE_ROLE_PLAYING: "Role-Playing",
APPLICATION_STORE_GENRE_DUNGEON_CRAWLER: "Dungeon Crawler",
APPLICATION_STORE_GENRE_ROGUELIKE: "Roguelike",
APPLICATION_STORE_GENRE_SHOOTER: "Shooter",
APPLICATION_STORE_GENRE_LIGHT_GUN: "Light-Gun",
APPLICATION_STORE_GENRE_SHOOT_EM_UP: "Shoot 'Em Up",
APPLICATION_STORE_GENRE_FPS: "FPS",
APPLICATION_STORE_GENRE_DUAL_JOYSTICK_SHOOTER: "Dual-Joystick Shooter",
APPLICATION_STORE_GENRE_SIMULATION: "Simulation",
APPLICATION_STORE_GENRE_FLIGHT_SIMULATOR: "Flight Simulator",
APPLICATION_STORE_GENRE_TRAIN_SIMULATOR: "Train Simulator",
APPLICATION_STORE_GENRE_LIFE_SIMULATOR: "Life Simulator",
APPLICATION_STORE_GENRE_FISHING: "Fishing",
APPLICATION_STORE_GENRE_SPORTS: "Sports",
APPLICATION_STORE_GENRE_BASEBALL: "Baseball",
APPLICATION_STORE_GENRE_BASKETBALL: "Basketball",
APPLICATION_STORE_GENRE_BILLIARDS: "Billiards",
APPLICATION_STORE_GENRE_BOWLING: "Bowling",
APPLICATION_STORE_GENRE_BOXING: "Boxing",
APPLICATION_STORE_GENRE_FOOTBALL: "Football",
APPLICATION_STORE_GENRE_GOLF: "Golf",
APPLICATION_STORE_GENRE_HOCKEY: "Hockey",
APPLICATION_STORE_GENRE_SKATEBOARDING_SKATING: "Skateboarding/Skating",
APPLICATION_STORE_GENRE_SNOWBOARDING_SKIING: "Snowboarding/Skiing",
APPLICATION_STORE_GENRE_SOCCER: "Soccer",
APPLICATION_STORE_GENRE_TRACK_FIELD: "Track & Field",
APPLICATION_STORE_GENRE_SURFING_WAKEBOARDING: "Surfing/Wakeboarding",
APPLICATION_STORE_GENRE_WRESTLING: "Wrestling",
APPLICATION_STORE_GENRE_STRATEGY: "Strategy",
APPLICATION_STORE_GENRE_FOUR_X: "4X",
APPLICATION_STORE_GENRE_ARTILLERY: "Artillery",
APPLICATION_STORE_GENRE_RTS: "RTS",
APPLICATION_STORE_GENRE_TOWER_DEFENSE: "Tower Defense",
APPLICATION_STORE_GENRE_TURN_BASED_STRATEGY: "Turn Based Strategy",
APPLICATION_STORE_GENRE_WARGAME: "Wargame",
APPLICATION_STORE_GENRE_MOBA: "MOBA",
APPLICATION_STORE_GENRE_FIGHTING: "Fighting",
APPLICATION_STORE_GENRE_PUZZLE: "Puzzle",
APPLICATION_STORE_GENRE_CARD_GAME: "Card Game",
APPLICATION_STORE_GENRE_EDUCATION: "Education",
APPLICATION_STORE_GENRE_FITNESS: "Fitness",
APPLICATION_STORE_GENRE_GAMBLING: "Gambling",
APPLICATION_STORE_GENRE_MUSIC_RHYTHM: "Music/Rhythm",
APPLICATION_STORE_GENRE_PARTY_MINI_GAME: "Party/Mini-Game",
APPLICATION_STORE_GENRE_PINBALL: "Pinball",
APPLICATION_STORE_GENRE_TRIVIA_BOARD_GAME: "Trivia/Board Game",
APPLICATION_STORE_GENRE_MISCELLANEOUS: "Other",
```

## About Content Rating Agencies

This is the list of content rating agencies, extracted from Discord's JS code.

```js
{
    ESRB: "1",
    PEGI: "2"
}
```

## About Entitlement Types

This is the list of Entitlement Types, extracted from Discord's JS code.

```js
{
    PURCHASE: 1, // Buying a store game
    PREMIUM_SUBSCRIPTION: 2, // Granted access to by Nitro
    DEVELOPER_GIFT: 3
```

## About SKU Types

This is the list of SKU Types, extracted from Discord's JS code.

```js
{
    GAME: 1,
    DLC: 2,
    CONSUMABLE: 3,
    BUNDLE: 4
}
```

## About SKU Access Types

This is the list of SKU Access Types, extracted from Discord's JS code.

```js
{
    FULL: 1,
    EARLY_ACCESS: 2,
    VIP_ACCESS: 3
}
```

## About OS Types

This is the list of Operating Systems, extracted from Discord's JS code.

```js
{
    WINDOWS: "1",
    MACOS: "2",
    LINUX: "3"
}
```

## About News Flags

This is the list of News Flags, extracted from Discord's JS code.

```js
{
    PATCH_NOTES: 2,
    PROMOTION: 4
}
```

## More fancy data

I didn't get around to documenting what these are for, but they're here in case someone needs them.

```
t.StoreDirectoryTileServerTypes = Object.freeze({
            REGULAR: 1,
            MEDIA: 2,
            STAFF: 3,
            BLOG: 4
        }), t.StoreDirectoryHeroCarouselItemTypes = Object.freeze({
            APPLICATION: "1",
            PREMIUM_ANNOUNCEMENT: "2"
        }), t.StoreDirectoryHeroCarouselItemServerTypes = Object.freeze({
            APPLICATION: 1,
            PREMIUM_ANNOUNCEMENT: 2
        }), t.DirectoryLocations = Object.freeze({
            HOME: "1",
            BROWSE: "2"
        }), t.StoreApplicationTypeFilter = Object.freeze({
            ALL: "all",
            PREMIUM: "nitro",
            DISTRIBUTION: "distribution"
        }), t.StoreApplicationGenresFilter = Object.freeze(s({
            ALL: -1
        }, x)), t.StoreApplicationSort = Object.freeze({
            FEATURED: "featured",
            NEW_RELEASES: "new",
            ALPHABETICAL: "desc",
            REVERSE_ALPHABETICAL: "asc",
            PREMIUM: "nitro"
        }), t.StoreApplicationPriceType = Object.freeze({
            PREMIUM: "1",
            DISTRIBUTION: "2",
            COMING_SOON: "3"
        }), t.BuildPlatformTypes = Object.freeze({
            WIN32: "win32",
            WIN64: "win64",
            MACOS: "macos",
            LINUX: "linux"
        }), t.GameNewsFlags = Object.freeze(), t.ReviewAggregators = Object.freeze({
            METACRITIC: 1
```
