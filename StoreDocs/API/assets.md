# Discord Store Assets

**This file is a part of Begonecord by ao.**

## Store Directory Assets

Can be accessed in form of `https://cdn.discordapp.com/store-directory-assets/applications/$appid/$filename`

Example: `https://cdn.discordapp.com/store-directory-assets/applications/487564993236172802/hero-background.jpg`

## Store App Assets & Videos

Can be accessed in form of `https://cdn.discordapp.com/app-assets/$appid/store/$filename`

Example: `https://cdn.discordapp.com/app-assets/363412675650125825/store/499022646424371210.mp4`

