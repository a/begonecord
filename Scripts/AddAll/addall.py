# This file is part of Begonecord, released under GPLv2
# https://gitlab.com/ao/beconecord_exploit

import time
import sys

try:
    import requests
except ModuleNotFoundError:
    print("You should install python-requests from pip.")
    sys.exit(1)

h = {"Authorization": "<your token>",
     "Content-Type": "application/json"}

storegames = requests.get("https://discordapp.com/api/v6/store/directory/497223016783151165?country_code=US")
storej = storegames.json()

ownedgames = requests.get(url="https://discordapp.com/api/v6/users/@me/library",
                         headers=h)
ownedj = ownedgames.json()

nitrostater = requests.get(url="https://discordapp.com/api/v6/users/@me/billing/subscriptions",
                           headers=h)
nitrostate = bool(len(nitrostater.json()))

ownedskus = []

for ownedlisting in ownedj:
    skuid = ownedlisting['sku_id']
    ownedskus.append(skuid)

print(f"Found {len(storej['store_listings'])} games, "
      f"we already own {len(ownedskus)}.")

for listing in storej["store_listings"]:
    skuid = listing['sku']['id']
    is_nitro = listing["sku"]["premium"]
    is_free = nitrostate if is_nitro else not listing["sku"]["price"]["amount"]
    if is_free and (skuid not in ownedskus):
        buyurl = "https://discordapp.com/api/v6/store/skus"\
                 f"/{skuid}/purchase"
        requests.post(url=buyurl, headers=h)
        print(f"Bought {listing['sku']['name']}, sleeping for 5 seconds")
        time.sleep(5)
