# This file is part of Begonecord, released under GPLv2
# https://gitlab.com/ao/beconecord_exploit

# !! PLEASE SET THE FOLLOWING !!

filename = "SuperMeatBoy_Release.exe"

appid_before = 425777385103949834
appid_after = 448417850466762753

# Please don't touch the following if you don't know what you're doing

piece_size = 4096  # 4 KiB

bappid_bytes = appid_before.to_bytes(8, byteorder='little')
aappid_bytes = appid_after.to_bytes(8, byteorder='little')

print("Starting")
print("Begonecord binary appid patcher by ao, up and running.")
print("This is the public release version (https://gitlab.com/"
      "ao/begonecord_exploit). Have fun.")
print()

# Based on https://stackoverflow.com/a/6787259/3286892
# by jeremy (https://stackoverflow.com/users/1114/jeremy)
with open(filename, "rb") as in_file,\
        open(f"patched_{filename}", "wb") as out_file:
    while True:
        piece = in_file.read(piece_size)

        if not piece:
            break  # end of file

        if bappid_bytes in piece:
            piece = piece.replace(bappid_bytes, aappid_bytes)
            print("Replaced bytes")

        out_file.write(piece)

print("Done")
