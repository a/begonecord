# This file is part of Begonecord, released under GPLv2
# https://gitlab.com/ao/beconecord_exploit

import json
import sys
import base64
import re

try:
    import win32pipe
    import win32file
except ModuleNotFoundError:
    print("You should install pywin32 from pip.")
    sys.exit(1)

TICKET_DUMP_FILENAME = "discordtickets.json"
TICKET_DB = {}


def read_from_json_file(filename):
    # Based on https://stackoverflow.com/a/20199213
    # by ubomb (https://stackoverflow.com/users/1546993)
    with open(filename) as json_data:
        return json.load(json_data)


def get_userid_from_ticket(ticket):
    ticket_parts = ticket.split('$')
    ticket_data = json.dumps(base64.b64decode(ticket_parts[-1]))
    return ticket_data['user_id']


def extract_client_id(ipc_message):
    # TODO: hacky
    extraction_regex = r"client_id\":\"([0-9]*)"
    return re.findall(extraction_regex, ipc_message.decode("utf-8"))[0]


def get_ticket(appid):
    if appid in TICKET_DB:
        return appid
    else:
        raise RuntimeError(f"No ticket found for {appid}")


def begonecord_drm2_with_backup():
    # The code is a bit messy, I'm sorry.

    # The IPC code is based on https://stackoverflow.com/a/51239081/3286892
    # By ChrisWue, https://stackoverflow.com/users/220986/chriswue
    pipe_name = r'\\?\pipe\discord-ipc-0'

    pipe = win32pipe.CreateNamedPipe(
        pipe_name,
        win32pipe.PIPE_ACCESS_DUPLEX,
        win32pipe.PIPE_TYPE_MESSAGE | win32pipe.PIPE_READMODE_MESSAGE | win32pipe.PIPE_WAIT,
        1, 65536, 65536,
        0,
        None)
    try:
        print("Begonecord: Waiting for game")
        win32pipe.ConnectNamedPipe(pipe, None)
        print("Begonecord: Game is here")
        resp = win32file.ReadFile(pipe, 64 * 1024)  # initial connect
        print(f"Begonecord Received: {resp}")

        client_id = extract_client_id(resp)
        print(f"Searching ticket for {client_id}")
        ticket = get_ticket(client_id)
        user_id = get_userid_from_ticket(ticket)

        # DO NOT TOUCH THIS UNLESS YOU KNOW WHAT YOU'RE DOING
        user_name = b'a' * (27 - len(user_id))

        client_data = b'\x01\x00\x00\x00\x13\x01\x00\x00{"cmd":"DISPATCH","data":{"v":1,"config":{"cdn_host":"cdn.discordapp.com","api_endpoint":"//discordapp.com/api","environment":"production"},"user":{"id":"' + user_id + b'","username":"' + user_name + b'","discriminator":"0001","avatar":null,"bot":false}},"evt":"READY","nonce":null}'
        validation_data = b'\x01\x00\x00\x00g\x01\x00\x00{"cmd":"GET_ENTITLEMENT_TICKET","data":{"ticket":"' + ticket + b'"},"evt":null,"nonce":"1"}'

        win32file.WriteFile(pipe, client_data)
        print(f"Begonecord Sent: {client_data}")
        resp = win32file.ReadFile(pipe, 64 * 1024)  # validation request
        print(f"Begonecord Received: {resp}")
        win32file.WriteFile(pipe, validation_data)
        print(f"Begonecord Sent: {validation_data}")
        win32file.CloseHandle(pipe)

        print("Begonecord: Finished")
    finally:
        win32file.CloseHandle(pipe)


if __name__ == '__main__':
    print("Begonecord (with automatic backed up ticket loading) for Discord Store DRM2 by ao, up and running.")
    print("This is the public release version (https://gitlab.com/ao/begonecord_exploit). Have fun.")
    print("You need to have your discord client closed for this to work properly btw due to the IPC magic stuffs.")
    print()
    global TICKET_DB
    TICKET_DB = read_from_json_file(TICKET_DUMP_FILENAME)
    begonecord_drm2_with_backup()
