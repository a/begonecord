# This file is part of Begonecord, released under GPLv2
# https://gitlab.com/ao/beconecord_exploit

import time
import json
import os
import sys

try:
    import requests
except ModuleNotFoundError:
    print("You should install python-requests from pip.")
    sys.exit(1)


HEADERS = {"Authorization": "<your token>",
           "Content-Type": "application/json"}

TICKET_DUMP_FILENAME = "discordtickets.json"

TICKETURL_TEMPLATE = "https://discordapp.com/api/v6/users/@me"\
                     "/applications/{}/entitlement-ticket"
LIBRARYURL = "https://discordapp.com/api/v6/users/@me/library"

RESULT_TICKETS = {}


def get_owned_appids():
    ownedgames = requests.get(url=LIBRARYURL,
                              headers=HEADERS)
    ownedj = ownedgames.json()

    ownedappids = []

    for ownedlisting in ownedj:
        appid = ownedlisting['application']['id']
        ownedappids.append(appid)

    return ownedappids


def get_ticket(appid):
    ticketurl = TICKETURL_TEMPLATE.format(appid)
    ticket = requests.post(url=ticketurl, headers=HEADERS)
    ticketj = ticket.json()
    return ticketj["ticket"]


def write_to_json_file(filename, filecontent):
    # Based on https://stackoverflow.com/a/26057360
    # by moobi (https://stackoverflow.com/users/2904083)
    with open(filename, 'w') as json_file:
        json.dump(filecontent, json_file)


def read_from_json_file(filename):
    # Based on https://stackoverflow.com/a/20199213
    # by ubomb (https://stackoverflow.com/users/1546993)
    with open(filename) as json_data:
        return json.load(json_data)


# Get our owned appids from library
ownedappids = get_owned_appids()
print(f"We own {len(ownedappids)} games, getting tickets for them now.")

# Check if we already have a dump, if we do, load it.
if os.path.isfile(TICKET_DUMP_FILENAME):
    RESULT_TICKETS = read_from_json_file(TICKET_DUMP_FILENAME)
    print(f"{len(RESULT_TICKETS)} tickets skipped as we already have them.")

# Go through all owned appids to dump tickets
for ownedappid in ownedappids:
    # If any ticket is already dumped, skip them
    if ownedappid in RESULT_TICKETS:
        continue

    # Get ticket, add it to a list and write it to a file
    RESULT_TICKETS[ownedappid] = get_ticket(ownedappid)
    write_to_json_file(TICKET_DUMP_FILENAME, RESULT_TICKETS)

    print(f"Got ticket for {ownedappid}, sleeping for 5 seconds")
    time.sleep(5)
