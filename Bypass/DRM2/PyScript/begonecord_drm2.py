try:
    import win32pipe
    import win32file
except ModuleNotFoundError:
    print("You should install pywin32 from pip.")


TICKET = b"1$<part1>$<part2>"  # Ticket that you got from API
USER_ID = b"449352724082065408"  # Account that got the ticket from API


def begonecord_drm2(ticket, user_id):
    # The code is a bit messy, I'm sorry.

    # The IPC code is based on https://stackoverflow.com/a/51239081/3286892
    # By ChrisWue, https://stackoverflow.com/users/220986/chriswue

    # DO NOT TOUCH THIS UNLESS YOU KNOW WHAT YOU'RE DOING
    user_name = b'a' * (27 - len(user_id))

    client_data = b'\x01\x00\x00\x00\x13\x01\x00\x00{"cmd":"DISPATCH","data":{"v":1,"config":{"cdn_host":"cdn.discordapp.com","api_endpoint":"//discordapp.com/api","environment":"production"},"user":{"id":"' + user_id + b'","username":"' + user_name + b'","discriminator":"0001","avatar":null,"bot":false}},"evt":"READY","nonce":null}'
    validation_data = b'\x01\x00\x00\x00g\x01\x00\x00{"cmd":"GET_ENTITLEMENT_TICKET","data":{"ticket":"' + ticket + b'"},"evt":null,"nonce":"1"}'
    pipe_name = r'\\?\pipe\discord-ipc-0'

    pipe = win32pipe.CreateNamedPipe(
        pipe_name,
        win32pipe.PIPE_ACCESS_DUPLEX,
        win32pipe.PIPE_TYPE_MESSAGE | win32pipe.PIPE_READMODE_MESSAGE | win32pipe.PIPE_WAIT,
        1, 65536, 65536,
        0,
        None)
    try:
        print("Begonecord: Waiting for game")
        win32pipe.ConnectNamedPipe(pipe, None)
        print("Begonecord: Game is here")

        resp = win32file.ReadFile(pipe, 64 * 1024)  # initial connect
        print(f"Begonecord Received: {resp}")
        win32file.WriteFile(pipe, client_data)
        print(f"Begonecord Sent: {client_data}")
        resp = win32file.ReadFile(pipe, 64 * 1024)  # validation request
        print(f"Begonecord Received: {resp}")
        win32file.WriteFile(pipe, validation_data)
        print(f"Begonecord Sent: {validation_data}")
        win32file.CloseHandle(pipe)

        print("Begonecord: Finished")
    finally:
        win32file.CloseHandle(pipe)


if __name__ == '__main__':
    print("Begonecord for Discord Store DRM2 by ao, up and running.")
    print("This is the public release version (https://gitlab.com/ao/begonecord_exploit). Have fun.")
    print("You need to have your discord client closed for this to work properly btw due to the IPC magic stuffs.")
    print()
    begonecord_drm2(TICKET, USER_ID)
