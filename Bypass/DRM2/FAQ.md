## Discord Store DRM2 FAQ

**This file is part of begonecord by ao.**

**Are tickets static for user/game combination?**

No, but they're reusable, so they're effectively static.

**Are tickets checked online? Can a game run offline?**

Nope, and yes, you can run a game offline.

**Do tickets share parts between two games owned by same person?**

Yes, second part is similar (which led to me discovering that it's base64).

**Do tickets share parts between two users on same game?**

Yes, second part is similar (which led to me discovering that it's base64).

**Can I simply replace old begonecord code with new one that returns ticket to play a game, offline, with client off?**

Yes, it works, even after restarts of game and system.

**Is the user_id checked against smth?**

Yes, it's checked against the userid we give in DISPATCH call.

**Is the appid checked against smth?**

Yes, game executable contains the appid, and this is given to us when RPC call is initiated. If you change appid in the base64, game refuses to boot, though that's because signature checks fail.

**Can a download of game can be ran by different userids?**

Yes, assuming they have a valid ticket and DISPATCH contains their userid.

