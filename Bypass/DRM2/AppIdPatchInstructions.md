## Discord Store Games: AppID Patch Instructions

**This file is part of Begonecord by ao.**

**You may just use `Scripts/PatchBinary` for this, this is the manual method.**

## Boring explanation

A simple way to get access to games that you can't currently get a ticket for (due to issues on discord's end, loss of access to account etc) is to simply... use a ticket from a different game.

While the appid of ticket *is* checked against the appid of the game and any modifications to the appid makes the signature check fail, there's no protections against editing the appid in the game.

So you can buy a free game or simply use a ticket for a game you already own, or even use someone else's valid ticket for any game (which would be piracy, please do not do that) as simply replacing the appid in the binary allows you to use any valid ticket for the resulting appid.

## Simple instructions

### First Time

0) Get a discord alt, note down the appid for the game you're trying to ([relevant API endpoint](https://gitlab.com/ao/begonecord_exploit/blob/master/StoreDocs/API/store-endpoints.md#get-apiv6storedirectory497223016783151165country_codecc))
1) Buy a free game, note down the appid ([relevant API endpoint](https://gitlab.com/ao/begonecord_exploit/blob/master/StoreDocs/API/store-endpoints.md#post-apiv6storeskusskuidpurchase), random free game: Minion Masters, appid: 448417850466762753, skuid: 488607666231443456).
2) Get the ticket for that game, note it down. ([relevant API endpoint](https://gitlab.com/ao/begonecord_exploit/blob/master/StoreDocs/API/store-endpoints.md#post-apiv6usersmeapplicationsappidentitlement-ticket))
3) Open the executable of the game you're trying to patch with a hex editor, do an integer search for the appid for that game (I used hxd for this).
4) Replace the bytes with the appid of the game you're trying to patch with the appid of the free game. Again, [hxd helps here as it simply has a Int64 box](https://awo.oooooooooooooo.ooo/i/1cq925c2.png).
5) Insert your userid and your ticket into Begonecord DRM2 bypass pyscript, close discord if it is running and run the pyscript with python3.
6) Run the game.

### Future uses

1) Close discord if it is running and run the pyscript with python3.
2) Run the game.
