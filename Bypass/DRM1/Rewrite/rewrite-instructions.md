## Rewrite Instructions

**This file is a part of Begonecord by ao.**

- Firstly, get a proxy/rewrite/mitm tool, install the root cert, enable ssl proxying. I used charles proxy. There's also example.xml in this folder that you can import to Charles Proxy to get access to Super Meat Boy (you'll just need to put in your userid and get a copy of Super Meat Boy from Discord Store).
- Enable rewrites, add a rewrite rule for `https://discordapp.com/api/v6/users/@me/applications/$appid/entitlements` (replace $appid with the game's appid).
- On that rewrite rule, rewrite the body with `[{ "user_id": "$userid", "sku_id": "$skuid", "application_id": "$appid","id": "500000000000000000","type": 1}]` (replace $userid with your user id, $skuid with the game's skuid, $appid with game's appid)
- Add a rewrite rule for `https://discordapp.com/api/v6/users/@me/library`, replace the body with a string like this (you might find relevant info on `/api/v6/applications` or on `/api/v6/users/@me/library` of a user that owns the game):

```js
[
  {
    "application": {
      "executables": [
        {
          "os": "win32",
          "name": "super meat boy/supermeatboy.exe"
        },
        {
          "os": "win32",
          "name": "supermeatboy.exe"
        }
      ],
      "publishers": [
        "Team Meat"
      ],
      "name": "Super Meat Boy",
      "distributor_applications": [
        {
          "distributor": "discord",
          "sku": "471500856043372544"
        },
        {
          "distributor": "steam",
          "sku": "40800"
        }
      ],
      "summary": "begonecord dabs on discord",
      "splash": null,
      "developers": [
        "Team Meat"
      ],
      "id": "425777385103949834",
      "icon": "16223d2622f9ee7e9854711cf5e6cd41"
    },
    "created_at": "2018-10-18T07:41:28.326000+00:00",
    "sku_id": "471500856043372544",
    "flags": 24,
    "branch_id": "425777385103949834"
  }
]
```
- Restart client
- Launch the game
- Have fun
